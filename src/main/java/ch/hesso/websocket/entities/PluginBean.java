/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.entities;

import ch.hesso.commons.AbstractEntity;
import ch.hesso.commons.AbstractEntityBuilder;
import ch.hesso.commons.HTTPMethod;
import ch.hesso.websocket.entities.visitors.WSVisitable;
import ch.hesso.websocket.entities.visitors.WSVisitor;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.websocket.Session;
import java.util.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PluginBean extends AbstractEntity implements WSVisitable {

	//
	private String name;

	//
	private String description;

	//
	private String webSocket;

	//
	private Map<String, String> webSockets;

	//
	private Map<String, Set<HTTPMethod>> registrations;

	//
	@JsonIgnore
	private transient Map<String, Session> _session = new HashMap<> ();

	//
	private PluginCommunication communication;

	/**
	 * @return
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName ( final String name ) {
		this.name = name;
	}

	/**
	 * @return
	 */
	public String getWebSocket () {
		return webSocket;
	}

	/**
	 * @param webSocket
	 */
	public void setWebSocket ( final String webSocket ) {
		this.webSocket = webSocket;
	}

	/**
	 * @return
	 */
	public String getDescription () {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription ( final String description ) {
		this.description = description;
	}

	/**
	 * @return
	 */
	public Map<String, String> getWebSockets () {
		return webSockets;
	}

	/**
	 * @param webSockets
	 */
	public void setWebSockets ( final Map<String, String> webSockets ) {
		this.webSockets = webSockets;
	}

	/**
	 * @return
	 */
	public Map<String, Set<HTTPMethod>> getRegistrations () {
		return registrations;
	}

	/**
	 * @param registrations
	 */
	public void setRegistrations ( final Map<String, Set<HTTPMethod>> registrations ) {
		this.registrations = registrations;
	}

	/**
	 * @return
	 */
	public Map<String, Session> getSession () {
		return _session;
	}

	/**
	 * @param session
	 */
	public void setSession ( final Map<String, Session> session ) {
		_session = session;
	}

	/**
	 * @param register
	 * @param session
	 */
	public void addSession ( final String register, final Session session ) {
		_session.put ( register, session );
	}

	/**
	 * @return
	 */
	@Override
	public String toString () {
		return "PluginBean{" +
				"name='" + name + '\'' +
				", webSocket='" + webSocket + '\'' +
				", webSockets=" + webSockets +
				", registrations=" + registrations +
				'}';
	}

	@Override
	public void accept ( final WSVisitor visitor ) {
		visitor.visit ( this );
	}

	/**
	 *
	 */
	public static final class PluginCoder extends JSONCoder<PluginBean> {

	}

	/**
	 *
	 */
	public static final class Builder extends AbstractEntityBuilder<PluginBean> {

		//
		private final PluginBean _plugin;

		//
		private Builder () {
			_plugin = new PluginBean ();
			_plugin.setRegistrations ( new HashMap<String, Set<HTTPMethod>> () );
			_plugin.setSession ( new HashMap<String, Session> () );
			_plugin.setWebSockets ( new HashMap<String, String> () );
		}

		/**
		 * @param name
		 *
		 * @return
		 */
		public static Builder newBuilder ( final String name ) {
			return new Builder ().name ( name );
		}

		/**
		 * @param name
		 *
		 * @return
		 */
		public Builder name ( final String name ) {
			_plugin.setName ( name );
			return this;
		}

		/**
		 * @param ws
		 *
		 * @return
		 */
		public Builder localSocket ( final String ws ) {
			_plugin.setWebSocket ( ws );
			return this;
		}

		/**
		 * @param resource
		 * @param ws
		 *
		 * @return
		 */
		public Builder serverSocket ( final String resource, final String ws ) {
			_plugin.webSockets.put ( resource, ws );
			return this;
		}

		/**
		 * @param resource
		 * @param actions
		 *
		 * @return
		 */
		public Builder registration ( final String resource, final HTTPMethod... actions ) {
			if ( actions.length > 0 ) {
				_plugin.registrations.put ( resource, new HashSet<> ( Arrays.<HTTPMethod>asList ( actions ) ) );
			}
			return this;
		}

		/**
		 * @return
		 */
		@Override
		public PluginBean build () {
			return _plugin;
		}

		/**
		 * @param id
		 *
		 * @return
		 */
		@Override
		public Builder id ( final String id ) {
			_plugin.setId ( id );
			return this;
		}
	}
}
