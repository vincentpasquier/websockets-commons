/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javax.websocket.*;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public abstract class JSONCoder<T> implements Encoder.TextStream<T>, Decoder.TextStream<T> {

	//
	private final ObjectMapper _mapper = new ObjectMapper ();

	//
	private Class<T> _type;

	/**
	 * @param reader
	 *
	 * @return
	 *
	 * @throws DecodeException
	 * @throws IOException
	 */
	@Override
	public T decode ( final Reader reader ) throws DecodeException, IOException {
		return _mapper.reader ( _type ).readValue ( reader );
	}

	/**
	 * @param object
	 * @param writer
	 *
	 * @throws EncodeException
	 * @throws IOException
	 */
	@Override
	public void encode ( final T object, final Writer writer ) throws EncodeException, IOException {
		_mapper.writerWithType ( _type ).writeValue ( writer, object );
	}

	/**
	 * @param config
	 */
	@Override
	@SuppressWarnings ("unchecked") // Cast done knowingly
	public void init ( final EndpointConfig config ) {
		ParameterizedType $class = (ParameterizedType) this.getClass ().getGenericSuperclass ();
		Type $type = $class.getActualTypeArguments ()[ 0 ];
		if ( $type instanceof Class ) {
			// SW: $type is indeed a class instance
			_type = (Class<T>) $type;
		} else if ( $type instanceof ParameterizedType ) {
			// SW: $type is indeed a ParametrizedType
			// SW: also, getting a Raw Type wield a Class of type T
			_type = (Class<T>) ( ( (ParameterizedType) $type ).getRawType () );
		}
		_mapper.setSerializationInclusion ( JsonInclude.Include.NON_NULL );
		//_mapper.setSerializationInclusion ( JsonInclude.Include.NON_DEFAULT );
		_mapper.configure ( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false );
		_mapper.configure ( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
	}

	/**
	 *
	 */
	@Override
	public void destroy () {
	}

}
