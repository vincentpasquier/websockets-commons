/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.entities;

import ch.hesso.websocket.entities.visitors.WSVisitable;
import ch.hesso.websocket.entities.visitors.WSVisitor;

import java.util.Map;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PluginCommunication implements WSVisitable {

	private String execute;

	private Map<String, Boolean> booleans;

	private Map<String, Double> doubles;

	private Map<String, Long> longs;

	private Map<String, String> strings;

	private Map<String, String[]> lists;

	public String getExecute () {
		return execute;
	}

	public void setExecute ( final String execute ) {
		this.execute = execute;
	}

	public Map<String, Boolean> getBooleans () {
		return booleans;
	}

	public void setBooleans ( final Map<String, Boolean> booleans ) {
		this.booleans = booleans;
	}

	public Map<String, Double> getDoubles () {
		return doubles;
	}

	public void setDoubles ( final Map<String, Double> doubles ) {
		this.doubles = doubles;
	}

	public Map<String, Long> getLongs () {
		return longs;
	}

	public void setLongs ( final Map<String, Long> longs ) {
		this.longs = longs;
	}

	public Map<String, String> getStrings () {
		return strings;
	}

	public void setStrings ( final Map<String, String> strings ) {
		this.strings = strings;
	}

	public Map<String, String[]> getLists () {
		return lists;
	}

	public void setLists ( final Map<String, String[]> lists ) {
		this.lists = lists;
	}

	@Override
	public void accept ( final WSVisitor visitor ) {
		visitor.visit ( this );
	}

	public static final class Coder extends JSONCoder<PluginCommunication> {

	}
}
