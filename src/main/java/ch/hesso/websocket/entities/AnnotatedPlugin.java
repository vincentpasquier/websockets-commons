/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.entities;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class AnnotatedPlugin {

	private Map<String, Map<String, String>> booleans = new HashMap<> ();

	private Map<String, Map<String, String>> doubles = new HashMap<> ();

	private Map<String, Map<String, String>> lists = new HashMap<> ();

	private Map<String, Map<String, String>> longs = new HashMap<> ();

	private Map<String, Map<String, String>> strings = new HashMap<> ();

	private Map<String, Map<String, String>> methods = new HashMap<> ();

	private Map<String, Object> objects = new HashMap<> ();

	private State state;

	private String text;

	private int progress;

	private int totalProgress;

	private String labelProgress;

	private int subProgress;

	private int totalSubProgress;

	private String labelSubProgress;

	public void addBooleans ( final String name, final Map<String, String> member ) {
		booleans.put ( name, member );
	}

	public void addDoubles ( final String name, final Map<String, String> member ) {
		doubles.put ( name, member );
	}

	public void addLists ( final String name, final Map<String, String> member ) {
		lists.put ( name, member );
	}

	public void addLongs ( final String name, final Map<String, String> member ) {
		longs.put ( name, member );
	}

	public void addStrings ( final String name, final Map<String, String> member ) {
		strings.put ( name, member );
	}

	public void addMethods ( final String name, final Map<String, String> member ) {
		methods.put ( name, member );
	}

	public String getText () {
		return text;
	}

	public void setText ( final String text ) {
		this.text = text;
	}

	public Map<String, Map<String, String>> getBooleans () {
		return booleans;
	}

	public void setBooleans ( final Map<String, Map<String, String>> booleans ) {
		this.booleans = booleans;
	}

	public Map<String, Map<String, String>> getDoubles () {
		return doubles;
	}

	public void setDoubles ( final Map<String, Map<String, String>> doubles ) {
		this.doubles = doubles;
	}

	public Map<String, Map<String, String>> getLists () {
		return lists;
	}

	public void setLists ( final Map<String, Map<String, String>> lists ) {
		this.lists = lists;
	}

	public Map<String, Map<String, String>> getLongs () {
		return longs;
	}

	public void setLongs ( final Map<String, Map<String, String>> longs ) {
		this.longs = longs;
	}

	public Map<String, Map<String, String>> getStrings () {
		return strings;
	}

	public void setStrings ( final Map<String, Map<String, String>> strings ) {
		this.strings = strings;
	}

	public Map<String, Map<String, String>> getMethods () {
		return methods;
	}

	public void setMethods ( final Map<String, Map<String, String>> methods ) {
		this.methods = methods;
	}

	public Map<String, Object> getObjects () {
		return objects;
	}

	public void setObjects ( final Map<String, Object> objects ) {
		this.objects = objects;
	}

	public State getState () {
		return state;
	}

	public void setState ( final State state ) {
		this.state = state;
	}

	public int getProgress () {
		return progress;
	}

	public void setProgress ( final int progress ) {
		this.progress = progress;
	}

	public int getTotalProgress () {
		return totalProgress;
	}

	public void setTotalProgress ( final int totalProgress ) {
		this.totalProgress = totalProgress;
	}

	public String getLabelProgress () {
		return labelProgress;
	}

	public void setLabelProgress ( final String labelProgress ) {
		this.labelProgress = labelProgress;
	}

	public int getSubProgress () {
		return subProgress;
	}

	public void setSubProgress ( final int subProgress ) {
		this.subProgress = subProgress;
	}

	public int getTotalSubProgress () {
		return totalSubProgress;
	}

	public void setTotalSubProgress ( final int totalSubProgress ) {
		this.totalSubProgress = totalSubProgress;
	}

	public String getLabelSubProgress () {
		return labelSubProgress;
	}

	public void setLabelSubProgress ( final String labelSubProgress ) {
		this.labelSubProgress = labelSubProgress;
	}

	public static enum State {
		IDLE,
		RUNNING,
		DONE,
		ERROR
	}

	public static final class Coder extends JSONCoder<AnnotatedPlugin> {

	}

}
